import React from "react";
import { BrowserRouter, Routes, Route,Navigate  } from "react-router-dom";
import { AuthProvider, useAuth } from './Route/AuthContext';
import Login from "./Components/Auth/Login";
import Dashboard from "./Components/PageComponents/Dashboard";
import Ida from "./Components/Page/Ida";
import DynamicAnalysis from "./Components/PageComponents/DynamicAnalysis";
import VerifyCode from "./Components/Auth/VerifyCode";
import ErrorPage from "./Components/Auth/ErrorPage";

const PrivateRoute = ({ element, ...props }) => {
  const { isLoggedIn } = useAuth();
  
  return isLoggedIn ? element : <Navigate to="/login" />;
};
const App = () => {
  return (
    <AuthProvider>
    <BrowserRouter>
      <Routes>
        <Route index path="/" element={<Login />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/VerifyCode" element={<VerifyCode />} />
        <Route path="*" element={<ErrorPage />} />
         <Route path="/Dashboard" element={<PrivateRoute element={<Dashboard />} />} />
         <Route path="/Dashboard/:id" element={<PrivateRoute element={<Ida />} />} />
         <Route path="/DynamicAnalysis" element={<PrivateRoute element={<DynamicAnalysis />} />} />
      </Routes>
    </BrowserRouter>
    </AuthProvider>
  );
};
export default App;