//  export const URL = "http://localhost:3001/"
 export const URL = "https://internalautomation-api.vercel.app/"

//Page
export const Dashboard = `${URL}Dashboard`
export const DynamicAnalysisUrl = `${URL}DynamicAnalysis`
export const VerifyCode =`${URL}VerifyCode`
