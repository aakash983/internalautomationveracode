import React, { useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Button,
  Box,
  TextField,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { Alert } from "@mui/material";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    border: "2px solid gray",
  },
  cell: {
    border: "1px solid gray",
  },
  filterContainer: {
    display: "flex",
    justifyContent: "end",
    marginBottom: "10px",
  },
  select: {
    marginRight: "10px",
  },
  applyButton: {
    marginLeft: "10px",
    backgroundColor: "#6e5dc6!important",
    color: "#ffffff!important",
    "&:hover": {
      backgroundColor: "#6e5dc6!important",
    },
  },
}));

const DialogComponent = ({ open, handleClose,checkId }) => {
  const classes = useStyles();

  const [text, setText] = useState("");
  const [error, setError] = useState(false);
  const handleChange = (event) => {
    setText(event.target.value);
  };
  const handleSubmit = () => {
    // if (text.trim() === "") {
      if(checkId===101.22 || checkId===103.2){
      setError(true);
      setSuccessMessage(true);
      setText("");
  
    } else {
      setError(false);
     setSuccessMessage(true);
      setText("");
    }
  };
  const [successMessage, setSuccessMessage] = useState('');
  const handleCloseMessage = () => {
    setSuccessMessage('');
  };

  return (
    <Dialog maxWidth="100vh" open={open} onClose={handleClose}>
      <DialogTitle>Code Comparision</DialogTitle>
      <Divider />
      <DialogContent>
        <Box mt={1} p={2} border={3} height="100%" borderColor="#6e5dc6">
        {successMessage && (
           <Alert
           onClose={handleCloseMessage}
           severity={error ? "error" : "success"}
         >
           {error
             ? "Error: Internal Error 502"
             : "Success: Validation Success"}
         </Alert>
      )}
          <TextField
            multiline
            rows={5}
            variant="outlined"
            label="Code Comparision"
            value={text}
            onChange={handleChange}
            error={error}
          />
          <br />
          <Button
            sx={{ marginTop: "10px" }}
            variant="text"
            onClick={handleSubmit}
          >
            Scan
          </Button>
        </Box>
        <Divider />
        <br />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button
            className={classes.applyButton}
            onClick={handleClose}
            variant="outlined"
          >
            Close
          </Button>
        </div>
      </DialogContent>
    </Dialog>
  );
};
export default DialogComponent;
