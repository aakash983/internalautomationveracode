import React, { useState,useEffect } from "react";
import {
  Dialog,
  DialogContent,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Typography,
  DialogTitle,
  Divider,
  Button,
  Paper,
  TableContainer,
  Select,
  MenuItem,Grid
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { NavLink } from "react-router-dom";
import ReportProblemIcon from '@mui/icons-material/ReportProblem';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    border: "2px solid gray",
  },
  cell: {
    border: "1px solid gray",
  },
  filterContainer: {
    display: "flex",
    justifyContent: "end",
    marginBottom: "10px",
  },
  select: {
    marginRight: "10px",
  },
  applyButton: {
    marginLeft: "10px",
    backgroundColor: "#1dd4f3!important",
    color: "#ffffff!important",
    "&:hover": {
      backgroundColor: "#1dd4f3!important",
    },
  },
}));

const DialogCom = ({ open, handleClose,onClose }) => {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [analysis, setAnalysis] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      //  const response = await fetch("http://localhost:3000/analysis");
      const response = await fetch("https://internalautomation-api.vercel.app/api/analysis");
      const data = await response.json();
      setAnalysis(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const handleRowClick = (id) => {
    window.open(`/Dashboard/${id}`);
    handleClose(false);
  //  onClose(null);
  };
  const [filter, setFilter] = useState({ field: "", value: "" });
  const [secondFilter, setSecondFilter] = useState([]);
  const handleChangeFilterField = (event) => {
    const field = event.target.value;
    setFilter({ field, value: "" });
    setSecondFilter(
      field ? Array.from(new Set(analysis.map((item) => item[field]))) : []
    );
  };

  const handleChangeFilterValue = (event) => {
    setFilter({ ...filter, value: event.target.value });
  };
  const handleApplyFilter = () => {
    console.log("Filter applied:", filter);
  };
  const handleClearFilters = () => {
    setFilter({ field: "", value: "" });
    setSecondFilter([]);
  };

  return (
    <Dialog maxWidth="100vh" open={open} onClose={handleClose}>
      <DialogTitle>Test Analysis History</DialogTitle>
      <Divider />
      <DialogContent>
        <Typography>
          Click a link for more test details about the selected
          dummy analysis.These occurences are read-only.Any changes to the analysis or
          scan configuration only apply to future occurences.
        </Typography>

        <div className={classes.filterContainer}>
          <Typography sx={{ marginRight: "10px", marginTop: "10px" }}>
            Filter by
          </Typography>
          <Grid item xs={6}>
              <Select
                style={{ width: "200px", height: "40px" }}
                value={filter.field}
                onChange={handleChangeFilterField}
              >
                <MenuItem value="serial">Analysis Occurence ID</MenuItem>
                <MenuItem value="analysis_status">Analysis Status</MenuItem>
                <MenuItem value="actual_start">Actual Start</MenuItem>
                <MenuItem value="actual_end">Actual End</MenuItem>
                <MenuItem value="scheduled_start">Scheduled Start</MenuItem>
                <MenuItem value="scheduled_end">Scheduled End</MenuItem>
              </Select>

              <Select
                style={{ width: "150px", height: "40px", marginLeft: "10px" }}
                disabled={!filter.field}
                value={filter.value}
                onChange={handleChangeFilterValue}
              >
                <MenuItem value="">Select Option</MenuItem>
                {secondFilter.map((option, index) => (
                  <MenuItem key={index} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </Select>

              <Button onClick={handleApplyFilter} disabled={!filter.value}>
                Apply
              </Button>
              <Button onClick={handleClearFilters}>Clear</Button>
            </Grid>
        </div>
        <TableContainer component={Paper}>
          <Table className={classes.table}>
            <TableHead>
              {" "}
              <TableRow>
                <TableCell className={classes.cell}>
                  Analysis Occurence ID
                </TableCell>
                <TableCell className={classes.cell}>Analysis Status</TableCell>
                <TableCell className={classes.cell}>Actual Start</TableCell>
                <TableCell className={classes.cell}>Actual End</TableCell>
                <TableCell className={classes.cell}>Scheduled Start</TableCell>
                <TableCell className={classes.cell}>Scheduled End</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {analysis
               .filter(
                (item) =>
                  (!filter.field || item[filter.field] === filter.value) 
              )
              .map((row) => (
                <TableRow key={row.id}>
                  <TableCell key={row.id} onClick={() => handleRowClick(row.id)} className={classes.cell}><NavLink>{row.serial}</NavLink></TableCell>
                  <TableCell className={classes.cell}>{row.analysis_status}</TableCell>
                  <TableCell className={classes.cell}>{row.actual_start}</TableCell>
                  <TableCell className={classes.cell}>{row.actual_end}</TableCell>
                  <TableCell className={classes.cell}>{row.scheduled_start}</TableCell>
                  <TableCell className={classes.cell}>{row.scheduled_end}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={10}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={(event, newPage) => setPage(newPage)}
          onChangeRowsPerPage={(event) => {
            setRowsPerPage(parseInt(event.target.value, 10));
            setPage(0);
          }}
        />
        <Divider />
        <br />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button className={classes.applyButton} onClick={handleClose} variant="outlined">
            Close
          </Button>
        </div>
      </DialogContent>
    </Dialog>
  );
};
export default DialogCom;
