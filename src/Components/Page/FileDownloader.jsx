import React from "react";
import { Button } from "@mui/material";

const FileDownloader = ({ data, fileName }) => {
  const downloadFile = () => {
    const jsonContent = JSON.stringify(data, null, 2);
    const blob = new Blob([jsonContent], { type: "application/json" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = fileName || "data.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <Button
      sx={{ float: "right", marginTop: "-20px" }}
      variant="text"
      onClick={downloadFile}
    >
      Download Flaw Details
    </Button>
  );
};
export default FileDownloader;
