import React, { useState } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import HomeIcon from "@mui/icons-material/Home";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import SettingsIcon from "@mui/icons-material/Settings";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import LogoutIcon from "@mui/icons-material/Logout";
import { useAuth } from '../../Route/AuthContext';

const Header = () => {
  const { isLoggedIn, logout } = useAuth();
  const navigate = useNavigate();
  const handleLogoutClick = () => {
    navigate("/");
    setAnchorEl1(null);
  };
  const handleNav = () => {
    navigate("/Dashboard");
  };
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorEl1, setAnchorEl1] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick1 = (event) => {
    setAnchorEl1(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClose1 = () => {
    setAnchorEl1(null);
  };

  return (
    <AppBar position="static" sx={{ background: "#f6fdfb", color: "#333333" }}>
      <Toolbar>
        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
        <HomeIcon />
        </Typography>
        <Typography
          variant="body2"
          color="inherit"
          onClick={handleClick}
          sx={{ flexGrow: 1 }}
        > 
          My Portfolio{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Basic Information</MenuItem>
          <MenuItem onClick={handleClose}>Portfolio</MenuItem>
        </Menu>
        <Typography
          onClick={handleNav}
          variant="body2"
          component="div"
          sx={{ flexGrow: 1, cursor: "pointer" }}
        >
          Scans & Analysis{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
          Analytics{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
          Policies{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
          Security Training{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>

        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
          <PersonOutlineIcon fontSize="small" />{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
          <NotificationsNoneIcon fontSize="small" />{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Typography
          onClick={handleClick1}
          variant="body2"
          component="div"
          sx={{ flexGrow: 1 }}
        >
          <SettingsIcon fontSize="small" />{" "}
          <KeyboardArrowDownIcon
            fontSize="small"
            sx={{ position: "absolute" }}
          />
        </Typography>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl1}
          keepMounted
          open={Boolean(anchorEl1)}
          onClose={handleClose1}
        >
          <MenuItem onClick={handleClose}>Profile</MenuItem>
          {/* <MenuItem onClick={handleLogoutClick}> */}
          <MenuItem onClick={logout}>
           <LogoutIcon fontSize="small" />
            Logout
          </MenuItem>
        </Menu>

        <Typography variant="body2" component="div" sx={{ flexGrow: 1 }}>
          <HelpOutlineIcon fontSize="small" />
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
export default Header;
