import React from "react";
import { Link } from "react-router-dom";
import PageNotFound from "../../Assests/Image/PageNotFound.jpg";
import { makeStyles } from "@material-ui/core/styles";
import {
    Container,
   Box,
    Typography,
  } from "@mui/material";

const useStyles = makeStyles((theme) => ({
    pageNotFound: {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        display: "block"
      },
}));
const ErrorPage = () => {
    const classes = useStyles();
  return (
    <Container>
      <Typography variant="h6" textAlign="center">
        The page you were looking for is not found!
      </Typography>
      <Typography variant="body1" textAlign="center">
        You may have mistyped the address or the page may have moved.
      </Typography>
      <Typography variant="button" sx={{display:"flex",justifyContent:"center"}}>
        <Link to="/Login">
          Back to Login
        </Link>
      </Typography>
      <img src={PageNotFound} className={classes.pageNotFound} />
    </Container>
  );
};
export default ErrorPage;
