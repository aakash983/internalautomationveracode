import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Container, Paper, Typography, Button } from "@material-ui/core";
import OTPInput from "react-otp-input";
import { useAuth } from '../../Route/AuthContext';

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
  },
  paper: {
    padding: theme.spacing(3),
    maxWidth: 400,
    gap: theme.spacing(2),
    borderRadius: "20px",
    background: "#1dd4f3",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    gap: theme.spacing(2),
  },
  customButton: {
    backgroundColor: "#92288c",
    borderRadius: "20px",
    color: "white",
    "&:hover": {
      backgroundColor: "#92288c",
    },
  },
  customText: {
    color: "#92288c",
    textAlign: "center",
    fontWeight: "bold",
  },
  inputOtpStyle: {
    width: "34px!important",
    height: "34px",
    borderRadius: "8px",
    border: "0px",
    marginLeft: "12px",
    marginRight: "12px",
    fontSize: "20px",
  },
}));
const VerifyCode = () => {
  const { verifyOTP } = useAuth();
  const navigate = useNavigate();
  const location = useLocation();
  const generatedOTP = location.state?.otp || "";
  const emailProp = location.state?.email || "";
  const classes = useStyles();
  const [verificationCode, setVerificationCode] = useState("");
  const [error, setError] = useState("");
  const handleVerifyCode = async () => {
    if (
      verificationCode.trim() === "" ||
      verificationCode.length < 6 
      // || verificationCode != generatedOTP
    ) {
      setError("Enter Valid Verification code");
    } else {
      verifyOTP();
      navigate("/Dashboard", { replace: true });
    }
  };
  const handleChange = (verificationCode) => {
    setVerificationCode(verificationCode);
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.paper} elevation={3}>
        <Typography
          className={classes.customText}
          color="textPrimary"
          variant="h6"
          align="center"
        >
          Please enter the code we just sent to email-{emailProp} to verify
        </Typography>
        <br />
        <form className={classes.form}>
          <OTPInput
            onChange={handleChange}
            value={verificationCode}
            inputStyle={classes.inputOtpStyle}
            numInputs={6}
            inputType="tel"
            shouldAutoFocus
            renderSeparator={<span>-</span>}
            renderInput={(props) => <input {...props} />}
          />
          <Button
            variant="contained"
            className={classes.customButton}
            fullWidth
            onClick={handleVerifyCode}
          >
            Verify Code
          </Button>
          <Typography
            style={{ textAlign: "center", color: "darkred" }}
            variant="body1"
          >
            {error}
          </Typography>
        </form>
        <br />
        <Typography
          className={classes.customText}
          container="div"
          variant="body2"
        >
          Back to <Link to="/Login">Login</Link>
        </Typography>
      </Paper>
    </Container>
  );
};
export default VerifyCode;
