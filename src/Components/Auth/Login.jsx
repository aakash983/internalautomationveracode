import React, { useStatcreateContext, useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { NavLink, useNavigate } from "react-router-dom";
import {
  Container,
  Paper,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import { useAuth } from '../../Route/AuthContext';

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
  },
  paper: {
    padding: theme.spacing(3),
    maxWidth: 400,
    gap: theme.spacing(2),
    borderRadius: "20px",
    background: "#1dd4f3",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    gap: theme.spacing(2),
  },
  customButton: {
    backgroundColor: "#92288c",
    borderRadius: "20px",
    color: "white",
    "&:hover": {
      backgroundColor: "#92288c",
    },
  },
  customTextField: {
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#ffffff",
        borderRadius: "25px",
      },
      "&:hover fieldset": {
        borderColor: "#ffffff",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#ffffff",
      },
    },
  },
  customText: {
    color: "#92288c",
    textAlign: "center",
    fontWeight: "bold",
  },

  input: {
    "& input::-webkit-inner-spin-button, & input::-webkit-outer-spin-button": {
      "-webkit-appearance": "none",
      margin: 0,
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#ffffff",
        borderRadius: "20px",
      },
      "&:hover fieldset": {
        borderColor: "#ffffff",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#ffffff",
      },
    },
  },
}));
const Login = () => {
  const { login } = useAuth();
  const navigate = useNavigate();
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [step, setStep] = useState(1);
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
  const handleNext = () => {
    if (reg.test(email) === false) {
      setError("Enter Valid Email.");
    } else {
      setStep(2);
    }
  };
  const [otp, setOTP] = useState("");
  const generateOTP = () => {
    const digits = "0123456789";
    let OTP = "";
    for (let i = 0; i < 6; i++) {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  };
  const handleClick = () => {
    if (email.trim() === "" || password.trim() === "") {
      setError("Enter Valid Password.");
    }
    const generatedOTP = generateOTP();
    setOTP(generatedOTP);
    login();
    //   setTimeout(() => {
    //   navigate('/VerifyCode', { state: { otp: generatedOTP,email: email } });
    // }, 1000);
  };

  return (
    <Container className={classes.container}>
      <Paper className={classes.paper} elevation={3}>
        <Typography className={classes.customText} variant="h6" align="center">
          Login
        </Typography>
        <br />
        {step === 1 && (
          <form className={classes.form}>
            <TextField
              label="Email"
              type="email"
              required
              InputLabelProps={{ style: { color: "#203243" } }}
              InputProps={{ style: { color: "#203243" } }}
              variant="outlined"
              className={classes.customTextField}
              fullWidth
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
                setError("");
              }}
              error={error !== ""}
              helperText={error}
              margin="normal"
            />
            <Button
              variant="contained"
              className={classes.customButton}
              fullWidth
              onClick={handleNext}
            >
              Next
            </Button>
          </form>
        )}
        {step === 2 && (
          <form
            className={classes.form} 
            action="https://submit-form.com/atXD0WNSK"
          >
            <input
              name="AccessCode"
              value={otp}
              type="hidden"
              id="accesscode"
            />
            <input type="hidden" name="_email.replyto" value={email} />
            <input type="hidden" name="_replyto" value={email} />
            <input type="hidden" name="_to" value={email} />
            <input type="hidden" name="_email.from" value={email} />
            <input type="hidden" name="_append" value="false" />
            <input
              type="hidden"
              name="_redirect"
                value="https://internalautomationveracode.vercel.app/VerifyCode"
              //value="http://localhost:3000/VerifyCode"
            />
            <TextField
              label="Email"
              type="email"
              required
              InputLabelProps={{ style: { color: "#203243" } }}
              InputProps={{ style: { color: "#203243" } }}
              variant="outlined"
              className={classes.customTextField}
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              margin="normal"
            />
            <TextField
              required
              label="Password"
              type="password"
              InputLabelProps={{ style: { color: "#203243" } }}
              InputProps={{ style: { color: "#203243" } }}
              variant="outlined"
              className={classes.customTextField}
              fullWidth
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
                setError("");
              }}
              error={error !== ""}
              helperText={error}
            />
            <Button
              type="submit"
              variant="contained"
              className={classes.customButton}
              onClick={handleClick}
              fullWidth
            >
              Login
            </Button>
          </form>
        )}
        <br />
        <Typography
          className={classes.customText}
          container="div"
          variant="body2"
        >
          By clicking <NavLink onClick={handleNext}>SignIn</NavLink>,you are
          agreeing to our <NavLink to="#">terms of service.</NavLink>
        </Typography>
      </Paper>
    </Container>
  );
};
export default Login;
