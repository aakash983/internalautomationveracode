import {
  Container,
  Breadcrumbs,
  Typography,
  Button,
  Box,
  Grid,
  Menu,
  MenuItem,Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TablePagination, IconButton
} from "@mui/material";
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { styled } from "@mui/material/styles";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import React, { useState, useEffect } from "react";
import Header from "../Header/Header";
import { NavLink, useNavigate } from "react-router-dom";
import IntegrationInstructionsIcon from "@mui/icons-material/IntegrationInstructions";
import ReportProblemIcon from "@mui/icons-material/ReportProblem";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import DialogCom from "../Page/DialogCom";
import Moment from "moment";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    border: "2px solid gray",
  },
  cell: {
    border: "1px solid gray",
  },
  filterContainer: {
    display: "flex",
    justifyContent: "end",
    marginBottom: "10px",
  },
  select: {
    marginRight: "10px",
  },
  applyButton: {
    marginLeft: "10px",
    backgroundColor: "#6e5dc6!important",
    color: "#ffffff!important",
    "&:hover": {
      backgroundColor: "#6e5dc6!important",
    },
  },
  customButton: {
    backgroundColor: "#1dd4f3!important",
    color: "#ffffff!important",
    "&:hover": {
      backgroundColor: "#1dd4f3!important",
    },
  },
  flipIcon: {
    transform: 'scaleY(-1)',
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const Cur_Date = Moment().format( "D MMM YYYY" );
  const Item = styled(Paper)(() => ({
    backgroundColor: "#fffeff",
    padding: 8,
    textAlign: "center",
    color: "black",
  }));
  const navigate = useNavigate();
  const handleClicked = () => {
    navigate("/DynamicAnalysis", { replace: true });
  };
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [dialogOpen, setDialogOpen] = useState(false);
  const handleOpenDialog = () => {
    setDialogOpen(true);
  };
  const handleCloseDialog = () => {
    setDialogOpen(false);
  };
  //kk
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5); // Default rows per page

  const data = [
    { id: 1,url:"b53jh2hjhh2lj2jh2e201", name: 'b53jh2hjhh2lj2jh2e201',status:"Time Expired- Partial Results Available",vh:0,h:0,m:2,l:0, total: 1,startDate:Cur_Date, internalScanning: '' },

  ];

  const rowsPerPageOptions = [5, 10, 25]; // Define rows per page options

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const [anchorEl1, setAnchorEl1] = useState(null);

  const handleMenuClick = (event) => {
    setAnchorEl1(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl1(null);
  };

  return (
    <div>
      <Header />
      <Container>
        <br />
        <Breadcrumbs aria-label="breadcrumb">
          <Typography variant="subtitle1" sx={{color:"#1976d2"}}>
            All Dynamic Analysis /
            </Typography>
        </Breadcrumbs>
        <Typography variant="h6">
            3582 - DMP - Italy - Prod
            </Typography>
        <Box sx={{ display: "flex", justifyContent: "end" }}>
          <Button title="Analyze Code" variant="text" onClick={handleClicked}>
            <IntegrationInstructionsIcon fontSize="large" />
            View Analysis Results
          </Button>
          &nbsp;&nbsp;
          <Button
            className={classes.customButton}
            variant="contained"
            onClick={handleClick}
          >
            Analysis Actions <KeyboardArrowDownIcon fontSize="small" />
          </Button>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleOpenDialog}>
              View Test Analysis History
            </MenuItem>
            <DialogCom open={dialogOpen} handleClose={handleCloseDialog}  onClose={handleClose}/>
          </Menu>
        </Box>
        <Box sx={{ marginTop: "20px" }}>
          <Grid container spacing={4}>
            <Grid item xs={4}>
              <Item elevation={3}>
                <Typography variant="subtitle1" align="left">
                  Analysis Status
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Completed - verifying Results
                </Typography>
                <Typography variant="subtitle2" align="left">
                  The Dynamics Analysis has successfully completed. Veracode is
                  verifying the results,which will be available after the review
                  is complete.
                </Typography>
                <br />
                <br />
                <br />
                <br />
              </Item>
            </Grid>

            <Grid item xs={4}>
              <Item elevation={3}>
                <Typography variant="subtitle1" align="left">
                  Analysis Schedule
                </Typography>
                <Typography variant="subtitle2">
                  Schedule Start: 20 Jan 2024 at 04:15AM
                </Typography>
                <Typography variant="subtitle2">
                  Schedule End: 27 Jan 2024 at 02:15AM
                </Typography>
                <br />
                <Typography variant="subtitle2">
                  Actual Start: 20 Jan 2024 at 04:18AM
                </Typography>
                <Typography variant="subtitle2">
                  Actual End: 23 Jan 2024 at 12:14PM
                </Typography>
                <br />
                <Typography variant="subtitle2">
                  Duration: 3d 7h 55m 50s
                </Typography>
                <Typography variant="caption" align="left">
                  Frequency: Recurring
                </Typography>
              </Item>
            </Grid>

            <Grid item xs={4}>
              <Item elevation={3}>
                <Typography variant="subtitle1" align="left">
                  Additional Information
                </Typography>
                <Typography variant="subtitle2">
                  Subscription capacity: 50 URL scans at a time
                </Typography>
              </Item>
            </Grid>

            <Grid item xs={12}>
              <Item elevation={3}>
                <Typography variant="subtitle1" align="left">
                  Operator Control Panel
                </Typography>
                <Grid container spacing={4}>
                  <Grid item xs={6}>
                    <Typography variant="subtitle2">
                      Analysis ID: d26tttvbbvvvv22hh1jjj
                    </Typography>
                    <Typography variant="subtitle2">
                      Analysis Occurrence ID: c26tttvbbvvvv22hh1jjj
                    </Typography>
                  </Grid>
                </Grid>
              </Item>
            </Grid>

            <Grid item xs={12}>
              <Item elevation={3}>
                <Typography variant="subtitle1" align="left">
                  Dynamic Analysis Configuration
                </Typography>
                <Grid container spacing={4}>
                  <Grid item xs={6}>
                    <Typography variant="subtitle2" align="left">
                      Business Unit: Not available
                    </Typography>
                    <Typography variant="subtitle2" align="left">
                      Visibility: Security leads and teams
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="subtitle2"
                      container="div"
                      align="right"
                    >
                      BlockList: Not available
                    </Typography>
                    <Typography variant="subtitle2" align="right">
                      User Agent: Security leads and teams
                    </Typography>
                  </Grid>
                </Grid>
              </Item>
            </Grid>

            <Grid item xs={12}>
              <Item elevation={3}>
                <Typography variant="subtitle1" align="left">
                  URLs List
                </Typography>
                <Grid container spacing={4}>
                  <Grid item xs={12}>
                  <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>URL</TableCell>
              <TableCell>Application Name</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>VH</TableCell>
              <TableCell>H</TableCell>
              <TableCell>M</TableCell>
              <TableCell>L</TableCell>
              <TableCell>Total</TableCell>
              <TableCell>Start Date</TableCell>
              <TableCell>Internal Scanning</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
              <TableRow key={row.id}>
                 <TableCell>{row.url}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.vh}</TableCell>
                <TableCell>{row.h}</TableCell>
                <TableCell>{row.m}</TableCell>
                <TableCell>{row.l}</TableCell>
                <TableCell>{row.total}</TableCell>
                <TableCell>{row.startDate}</TableCell>
                <TableCell>{row.internalScanning}</TableCell>
        
                <TableCell>
                  <IconButton aria-label="more" onClick={handleMenuClick}>
                    <MoreVertIcon className={classes.flipIcon}/>
                  </IconButton>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl1}
                    open={Boolean(anchorEl1)}
                    onClose={handleMenuClose}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                  >
                    <MenuItem onClick={handleMenuClose}>Delete</MenuItem>
                    <MenuItem onClick={handleMenuClose}>Update</MenuItem>
                  </Menu>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={rowsPerPageOptions}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
                  </Grid>
                </Grid>
              </Item>
            </Grid>
          </Grid>
          <br />
          <br />
        </Box>
      </Container>
    </div>
  );
};
export default Dashboard;