import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Box,
  Container,
  Breadcrumbs,
  Grid,
  Typography,
  FormControl,
  Select,
  MenuItem,
  InputLabel,
  Button,
  Menu,
  IconButton,
  Radio,
  FormControlLabel,
  RadioGroup,
  FormLabel,
  Checkbox,
} from "@mui/material";
import { NavLink } from "react-router-dom";
import Header from "../Header/Header";
import LaunchIcon from "@mui/icons-material/Launch";
import Stack from "@mui/material/Stack";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import FileDownloader from "../Page/FileDownloader";
import dataForFirstButton from "../Page/dynamicFlaw-301.json";
import dataForSecondButton from "../Page/dynamicFlaw.json";
import dataForThirdButton from "../Page/dynamicFlaw-200.json";
import DialogComponent from "../Page/DialogComponent";
import CodeIcon from "@mui/icons-material/Code";
import DialogModal from "../Page/DialogModal";
const DynamicAnalysis = () => {
  const [selectedRows, setSelectedRows] = useState([]);
  const [responseData, setResponseData] = useState(null);
  const handleCheckboxChange = (row) => {
    const isSelected = selectedRows.includes(row.id);
    if (isSelected) {
      setSelectedRows(selectedRows.filter((id) => id !== row.id));
      setResponseData(null);
    } else {
      setSelectedRows([...selectedRows, row.id]);
      setResponseData(row);
    }
  };
  const [displayedData, setDisplayedData] = useState(true);
  const [detailData, setDetailData] = useState(true);
  const handleFirstButtonClick = () => {
    setDetailData(!detailData);
    setDisplayedData(false);
  };
  const handleSecondButtonClick = async () => {
    try {
      const res = await fetch(
        "https://internalautomation-api.vercel.app/api/posts/1"
      );
      if (!res.ok) {
        throw new Error("Failed to fetch data");
      }
      const data = await res.json();
      setDisplayedData(data);
      setDisplayedData(!displayedData);
      setDetailData(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      setDisplayedData(!displayedData);
      setDetailData(false);
    }
  };
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const response = await fetch(
        "https://internalautomation-api.vercel.app/api/posts"
      );
      const data = await response.json();
      setPosts(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const [selectedValue, setSelectedValue] = useState("");
  const [showButton, setShowButton] = useState(false);
  const handleChange = (event) => {
    setSelectedValue(event.target.value);
    setShowButton(true);
  };
  const handleSave = async (id, updatedPost) => {
    console.log("dddd");
    console.log("responseData", JSON.stringify(responseData));
    try {
      await fetch(
        `https://internalautomation-api.vercel.app/api/posts/${responseData.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            review_status: selectedValue,
            sev: responseData.sev,
            name: responseData.name,
            parameter: responseData.parameter,
            site: responseData.site,
            path: responseData.path,
            mitigation_status: responseData.mitigation_status,
          }),
        }
      );
      setPosts(posts.map((post) => (post.id === id ? updatedPost : post)));
      setSelectedValue("");
      window.location.reload();
    } catch (error) {
      console.error("Error updating post:", error);
    }
  };

  const [anchorEl, setAnchorEl] = useState(null);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [dialogOpen, setDialogOpen] = useState(false);
  const handleOpenDialog = () => {
    setDialogOpen(true);
  };

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  const [isDialogFinal, setDialogFinal] = useState(false);
  const [isDialogFinals, setDialogFinals] = useState(false);
  const handleFinalCloseDialog = () => {
    setDialogFinal(false);
  };
  const handleFinalCloseDialog1 = async (id, updatedPost) => {
    try {
      await fetch(
        `https://internalautomation-api.vercel.app/api/posts/${responseData.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            review_status: responseData.review_status,
            sev: responseData.sev,
            name: responseData.name,
            parameter: responseData.parameter,
            site: responseData.site,
            path: responseData.path,
            mitigation_status: responseData.mitigation_status,
            status: "published",
          }),
        }
      );
      setPosts(posts.map((post) => (post.id === id ? updatedPost : post)));
      alert("Published");
      window.location.reload();
    } catch (error) {
      console.error("Error updating post:", error);
    }
  };
  const handleFinalCloseDialog2 = async (id, updatedPost) => {
    try {
      await fetch(
        `https://internalautomation-api.vercel.app/api/posts/${responseData.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            review_status: responseData.review_status,
            sev: responseData.sev,
            name: responseData.name,
            parameter: responseData.parameter,
            site: responseData.site,
            path: responseData.path,
            mitigation_status: responseData.mitigation_status,
            status: "unpublished",
          }),
        }
      );
      setPosts(posts.map((post) => (post.id === id ? updatedPost : post)));
      alert("UnPublished");
      window.location.reload();
    } catch (error) {
      console.error("Error updating post:", error);
    }
  };
  const handleFinalCloseDialogs = () => {
    setDialogFinals(false);
  };

  const dialogContentFinal = (
    <div>
      <div>
        You are about results. Please confirm
        <Typography>Subscription: 2309-119753: DYN-URL</Typography>
        <Typography>Special Scan: Free SCan</Typography>
      </div>
    </div>
  );
  const dialogActionsFinal = (
    <div>
      <Button
        variant="contained"
        onClick={handleFinalCloseDialog1}
        color="primary"
      >
        Publish
      </Button>
      &nbsp;&nbsp;
      <Button
        variant="outlined"
        onClick={handleFinalCloseDialog}
        color="primary"
      >
        Cancel
      </Button>
    </div>
  );
  const dialogActionsFinals = (
    <div>
      <Button
        variant="contained"
        onClick={handleFinalCloseDialog2}
        color="primary"
      >
        UnPublish
      </Button>
      &nbsp;&nbsp;
      <Button
        variant="outlined"
        onClick={handleFinalCloseDialogs}
        color="primary"
      >
        Cancel
      </Button>
    </div>
  );
  const publishHandle = () => {
    setDialogFinal(true);
  };
  const publishHandles = () => {
    setDialogFinals(true);
  };
  const [filter, setFilter] = useState({ field: "", value: "" });
  const [secondFilter, setSecondFilter] = useState([]);
  const [additionalFilter, setAdditionalFilter] = useState("");
  const handleChangeFilterField = (event) => {
    const field = event.target.value;
    setFilter({ field, value: "" });
    setSecondFilter(
      field ? Array.from(new Set(posts.map((item) => item[field]))) : []
    );
  };

  const handleChangeFilterValue = (event) => {
    setFilter({ ...filter, value: event.target.value });
  };
  const handleApplyFilter = () => {
    console.log("Filter applied:", filter);
  };
  const handleClearFilters = () => {
    setFilter({ field: "", value: "" });
    setSecondFilter([]);
    setAdditionalFilter("");
  };
  const handleChangeAdditionalFilter = (event) => {
    setAdditionalFilter(event.target.value);
  };
  const [selectedRows1, setSelectedRows1] = useState([]);

  const handleCheckboxChange1 = (event, id, rowData) => {
    const updatedRows = posts.map((row) => {
      if (row.id === id) {
        return { ...row, selected: event.target.checked };
      }
      return row;
    });
    setPosts(updatedRows);
    if (event.target.checked) {
      setSelectedRows1([id]);
      setResponseData(rowData);
    } else {
      setSelectedRows1([]);
      setResponseData(null);
    }
  };

  const handleDownloadClick = () => {
    const jsonContent = JSON.stringify(dataForFirstButton, null, 2);
    const blob = new Blob([jsonContent], { type: "application/json" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = "DynamicFlaw.json" || "data.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };
  const htmlString =
    '<!DOCTYPE HTML PUBLIC "..//IETF//DTD HTML 2.0//EN"><html><head><title>301 Moved Permanently</title></head><body><h1>Move Permanently</h1><p>The document has moved <a href="https://appex-reviewer.com/port:3001">here</a>.</p></body></html>';

  return (
    <div>
      <Header />
      <Container>
        <br />
        <Stack spacing={2}>
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            <NavLink
              underline="hover"
              color="text.primary"
              href="#"
              aria-current="page"
            >
              Dynamic Analysis
            </NavLink>
          </Breadcrumbs>
        </Stack>
        <br />

        <Box style={{ display: "flex", justifyContent: "end" }}>
          <FormControl>
            <FormLabel>Show:</FormLabel>
            <RadioGroup
              defaultValue="flawdetails"
              row
              name="row-radio-buttons-group"
            >
              <FormControlLabel
                value="fixfirstanalyzer"
                control={<Radio />}
                label="Fix First Analyzer"
              />
              <FormControlLabel
                value="flawdetails"
                control={<Radio />}
                label="Flaw Details"
              />
              <FormControlLabel value="none" control={<Radio />} label="None" />
            </RadioGroup>
          </FormControl>
        </Box>
        <Typography variant="body1">Showing Results for 1 sites</Typography>
        {responseData && (
          <Box>
            <Typography sx={{ color: "blue" }} variant="subtitle1">
              Inclusion of Functionality from Untrusted Control Sphere{" "}
              <a href="#">
                CWE ID:{responseData.name}
                <LaunchIcon fontSize="small" />
              </a>
            </Typography>
            <Box>
              <IconButton
                sx={{ fontSize: "16px", marginLeft: "100px" }}
                onClick={handleOpenDialog}
                title="Code Comparision"
                size="large"
                color="inherit"
              >
                Code Comparision
                <CodeIcon fontSize="large" />
              </IconButton>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <DialogComponent
                  open={dialogOpen}
                  handleClose={handleCloseDialog}
                  checkId={responseData.name}
                />
              </Menu>
              {responseData.name == 100.22 &&
                <FileDownloader
                data={dataForFirstButton}
                fileName="DynamicFlaw.json"
              />
              }
              {responseData.name == 101.22 &&
                <FileDownloader
                data={dataForSecondButton}
                fileName="DynamicFlaw.json"
              />
              }
              {responseData.name == 103.2 &&
                <FileDownloader
                data={dataForThirdButton}
                fileName="DynamicFlaw.json"
              />
              }
            </Box>

            <Box mt={1} p={2} border={3} borderColor="#6e5dc6">
              <Grid item xs={12}>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Typography sx={{ color: "blue" }} variant="subtitle1">
                      Flaw URL:
                      <a href="#">
                        https://appex-reviewer.com/dashboard/tabContent/openOrders
                      </a>
                    </Typography>
                    <Typography sx={{ color: "blue" }} variant="subtitle1">
                      Method:
                      <span style={{ color: "GrayText" }}>GET</span>
                    </Typography>
                    <Typography sx={{ color: "blue" }} variant="subtitle1">
                      Protocols:
                      <span style={{ color: "GrayText" }}>Https</span>
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    {responseData.name == 100.22 && (
                      <Typography sx={{ color: "blue" }} variant="subtitle1">
                        Vulnerable Parameter:
                        <span style={{ color: "GrayText" }}>
                          {" "}
                          Content-Security-Policy
                        </span>
                      </Typography>
                    )}
                    <Typography sx={{ color: "blue" }} variant="subtitle1">
                      Original Value:
                      <span style={{ color: "GrayText" }}>
                        {" "}
                        [value missing]
                      </span>
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <div>
                <Button
                  variant="contained"
                  color="inherit"
                  onClick={handleFirstButtonClick}
                >
                  Details
                </Button>
                &nbsp;
                <Button
                  variant="outlined"
                  color="inherit"
                  onClick={handleSecondButtonClick}
                >
                  Request/Response
                </Button>
                {detailData && (
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Typography variant="subtitle1">Details:</Typography>
                      <Box sx={{ marginTop: "5px" }}>
                        <Typography sx={{ color: "blue" }} variant="subtitle1">
                          Description
                        </Typography>
                        <Typography variant="body1">
                          The target web page not specify
                          Content-Security-Policy. While lack of
                          Content-Security-Policy on the website does not
                          represents a security risk by itself, a strict
                          Content-Security-Policy may serve as an additional
                          protection layer from Cross-Site-Scripting attacks.
                          Content-Security-Policy helps to ensure that all
                          resources (such as scripts,images or video files) on
                          the website are loaded from trusted
                          sources.Content-Security-Policy may also help to
                          protect from UI redressing attacks,such as
                          "Clickjacking".
                        </Typography>
                        <Typography sx={{ color: "blue" }} variant="subtitle1">
                          Additonal Resources: CWE
                          <LaunchIcon fontSize="small" />
                        </Typography>
                        <Typography sx={{ color: "blue" }} variant="subtitle1">
                          Recommendations
                        </Typography>
                        <Typography variant="body1">
                          Consider implementing strict Content-Security-Policy
                          by setting the corresponding HTTP header for each page
                          response from the website. It is recommended to move
                          all scripts to trusted locations and specify
                          "script-src" or "default-src" directives.If inline
                          javascript is required on the websites,inline script
                          may be accompanied with a cryptographic nonce (number
                          used once),or a hash,to ensure only trusted scripts
                          are executed.
                        </Typography>
                      </Box>
                    </Grid>
                  </Grid>
                )}
                {displayedData && (
                  <Grid sx={{ marginTop: "10px" }} container spacing={2}>
                    <Grid item xs={6}>
                      <Box border={1} p={2}>
                        <Typography variant="h6">Request</Typography>
                        <Box>
                          <Typography variant="body1">
                            GET /posts/1 HTTP/1.1
                          </Typography>
                          <Typography variant="body1">
                            Host:{" "}
                            <a href="#">
                              https://appex-reviewer.com/dashboard/tabContent/openOrders
                            </a>
                          </Typography>
                          <Typography variant="body1">
                            Upgrade-Insecure-Requests: 1
                          </Typography>
                          <Typography variant="body1">
                            User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64;
                            x64) AppleWebKit/537.36 (KHTML, like Gecko)
                            Chrome/97.0.4692.99 Safari/537.36
                          </Typography>
                          <Typography variant="body1">Accept: */*</Typography>
                          <Typography variant="body1">
                            Sec-Fetch-Site: same-origin
                          </Typography>
                          <Typography variant="body1">
                            Sec-Fetch-Mode: cors
                          </Typography>
                        </Box>
                      </Box>
                    </Grid>
                    <Grid item xs={6}>
                      <Box border={1} p={2}>
                        <Typography variant="h6">Response</Typography>
                        <Box>
                          {responseData.name == 100.22 && (
                            <Typography variant="body1">
                              HTTP/1.1 301 Moved Permanently
                            </Typography>
                          )}
                          {responseData.name == 101.22 && (
                            <Typography>HTTP/1.1 500 internal error</Typography>
                          )}
                          {responseData.name == 103.2 && (
                            <Typography>HTTP/1.1 200 ok Sucessfully</Typography>
                          )}
                          <Typography variant="body1">
                            Content-Type: application/json; charset=utf-8
                          </Typography>
                          <Typography variant="body1">
                            Content-Length:{" "}
                            {JSON.stringify(displayedData).length}
                          </Typography>
                          <Typography variant="body1">
                            Connection: keep-alive
                          </Typography>
                          <Typography variant="body1">
                            Date: {new Date().toLocaleString()}
                          </Typography>
                          <Typography variant="body1">
                            Server: cloudflare
                          </Typography>
                          <Typography variant="body1">
                            Strict-Transport-Security: max-age=31536000;
                            includeSubDomains; preload
                          </Typography>
                          <Typography variant="body1">
                            Location:
                            https://jsonplaceholder.typicode.com/posts/1
                          </Typography>
                          <Typography variant="body1">
                            X-CDN: Incapsula
                          </Typography>
                          <Typography variant="body1">
                            Incap-Country-Code: US
                          </Typography>
                          <Typography variant="body1">
                            {/* <div dangerouslySetInnerHTML={{ __html: htmlString }} /> */}
                            {htmlString}
                          </Typography>
                        </Box>
                      </Box>
                    </Grid>
                  </Grid>
                )}
              </div>
            </Box>
          </Box>
        )}
        {!responseData && (
          <Box
            mt={1}
            p={2}
            border={3}
            height="100px"
            borderColor="#6e5dc6"
          ></Box>
        )}
        <Typography sx={{ display:"flex",justifyContent:"left",marginTop: "10px" }} variant="subtitle1">
          Flaws: 3 of 3
        </Typography>
      
        {responseData && (
        <Box sx={{ display: "flex", justifyContent: "end" }}>
          {" "}
          <Button
            disabled={responseData?.status == "published" ? true : false}
            onClick={publishHandle}
            variant="text "
          >
            Publish
          </Button>
          <Button
            disabled={responseData?.status == "unpublished" ? true : false}
            onClick={publishHandles}
            variant="text"
          >
            Unpublish
          </Button>
        </Box>)}
        <DialogModal
          open={isDialogFinal}
          onClose={handleFinalCloseDialog}
          title="Publish Dialog"
          content={dialogContentFinal}
          actions={dialogActionsFinal}
        />
        <DialogModal
          open={isDialogFinals}
          onClose={handleFinalCloseDialogs}
          title="UnPublish Dialog"
          content={dialogContentFinal}
          actions={dialogActionsFinals}
        />
          {selectedRows1.length > 0 && (
          // <Button variant="contained" onClick={handleDownloadClick}>
          //   Download Flaw Details
          // </Button>
          <Box sx={{display:"flex",justifyContent:"left",marginTop: "10px"}}>
               {responseData.name == 100.22 &&
                <FileDownloader
                data={dataForFirstButton}
                fileName="DynamicFlaw.json"
              />
              }
              {responseData.name == 101.22 &&
                <FileDownloader
                data={dataForSecondButton}
                fileName="DynamicFlaw.json"
              />
              }
              {responseData.name == 103.2 &&
                <FileDownloader
                data={dataForThirdButton}
                fileName="DynamicFlaw.json"
              />
              }
          </Box>
        )}
        <Box
          height="30px"
          sx={{
            marginTop: "20px",
            border: "1px solid #ccc",
            backgroundColor: "#c4e3e1",
            boxShadow: "0 2px 4px rgba(0,0,0,0,1)",
            padding: 2,
          }}
        >
          <Grid container spacing={1}>
            <Typography sx={{ position: "relative", alignContent: "center" }}>
              All 3 Flaws
            </Typography>
            <Grid item xs={2}>
              <Select
                style={{ width: "100px", height: "40px" }}
                value={additionalFilter}
                onChange={handleChangeAdditionalFilter}
              >
                <MenuItem value="Open">Open</MenuItem>
                <MenuItem value="Valid">Valid</MenuItem>
                <MenuItem value="Invalid">Invalid</MenuItem>
              </Select>
            </Grid>
            <InputLabel sx={{ position: "relative", alignContent: "center" }}>
              Search
            </InputLabel>
            <Grid item xs={6}>
              <Select
                style={{ width: "200px", height: "40px" }}
                value={filter.field}
                onChange={handleChangeFilterField}
              >
                <MenuItem value="serial">ID</MenuItem>
                <MenuItem value="sev">Veracode Severity</MenuItem>
                <MenuItem value="sev">Custom Severity</MenuItem>
                <MenuItem value="name">CWE Name</MenuItem>
                <MenuItem value="parameter">Vuln Parameter</MenuItem>
                <MenuItem value="name">CWE ID</MenuItem>
                <MenuItem value="site">Site</MenuItem>
                <MenuItem value="path">Path</MenuItem>
                <MenuItem value="review_status">Review Status</MenuItem>
                <MenuItem value="mitigation_status">Mitigation Status</MenuItem>
              </Select>

              <Select
                style={{ width: "150px", height: "40px", marginLeft: "10px" }}
                disabled={!filter.field}
                value={filter.value}
                onChange={handleChangeFilterValue}
              >
                <MenuItem value="">Select Option</MenuItem>
                {secondFilter.map((option, index) => (
                  <MenuItem key={index} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </Select>

              <Button onClick={handleApplyFilter} disabled={!filter.value}>
                Apply
              </Button>
              <Button onClick={handleClearFilters}>Clear</Button>
            </Grid>
            <Box>
              <FormControl variant="standard" sx={{ minWidth: 200 }}>
                <InputLabel>Mark Selected Flaws as:</InputLabel>
                <Select
                  value={selectedValue}
                  onChange={handleChange}
                  label="Mark Selected Flaws as:"
                >
                  <MenuItem value="FP">Mark False Positive</MenuItem>
                  <MenuItem value="Valid">Mark Valid</MenuItem>
                  <MenuItem value="Open">Mark Open</MenuItem>
                  <MenuItem value="Raise">Raise Exploitability</MenuItem>
                  <MenuItem value="Lower">Lower Exploitability</MenuItem>
                  <MenuItem value="Auto">Mark Flaws Valid Auto</MenuItem>
                  <MenuItem value="Internal">Mark Internal</MenuItem>
                </Select>
                {showButton && <Button onClick={handleSave}>Go</Button>}
              </FormControl>
            </Box>
          </Grid>
        </Box>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell>Serial Number</TableCell>
                <TableCell>VSev</TableCell>
                <TableCell>CWE ID & Name</TableCell>
                <TableCell>Vuln Parameter</TableCell>
                <TableCell>Site</TableCell>
                <TableCell>Path</TableCell>
                <TableCell>Validity</TableCell>
                <TableCell>Review Status</TableCell>
                <TableCell>Mitigation Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {posts
                .filter(
                  (item) =>
                    (!filter.field || item[filter.field] === filter.value) &&
                    (!additionalFilter ||
                      item.review_status === additionalFilter)
                )
                .map((row, index) => (
                  <TableRow key={row.id}>
                    <TableCell>
                      <Checkbox
                        disabled={selectedRows1.length > 0 && !row.selected}
                        checked={row.selected}
                        onChange={(e) => handleCheckboxChange1(e, row.id, row)}
                      />
                    </TableCell>
                    <TableCell>{row.serial}</TableCell>
                    <TableCell>{row.sev}</TableCell>
                    <TableCell onClick={() => handleCheckboxChange(row)}>
                      <NavLink>{row.name}</NavLink>
                    </TableCell>
                    <TableCell>{row.parameter}</TableCell>
                    <TableCell>{row.site}</TableCell>
                    <TableCell>{row.path}</TableCell>
                    <TableCell>
                      {row.review_status === "Open" ? "Open" : "Closed"}
                    </TableCell>
                    <TableCell>{row.review_status}</TableCell>
                    <TableCell>{row.mitigation_status}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    </div>
  );
};
export default DynamicAnalysis;
